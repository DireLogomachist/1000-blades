using UnityEngine;
using UnityEditor;
using System;

public class FBXScaleOverride : AssetPostprocessor {
	void OnPreprocessModel() {

		Debug.Log("Applying flat shading defaults...");
		ModelImporter importer = assetImporter as ModelImporter;
		String name = importer.assetPath.ToLower();
		if (name.Substring(name.Length - 6, 6) == ".blend") {
			importer.normalImportMode = ModelImporterTangentSpaceMode.Calculate;
			importer.normalSmoothingAngle = 0.0f;

			importer.importAnimation = false;
			importer.generateAnimations = ModelImporterGenerateAnimations.None;
			importer.animationType = ModelImporterAnimationType.None;
		}

	}
}
