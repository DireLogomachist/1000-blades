﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.ImageEffects;

public class GameManager : MonoBehaviour {

	bool startScreen = true;
	Vector3 startPos;
	Vector3 startRot;
	public Vector3 endPos;
	public Vector3 endRot;

	public GameObject sword;

	bool transition = false;
	bool avail = false;

	// Use this for initialization
	void Start () {
		startPos = gameObject.transform.position;
		startRot = gameObject.transform.rotation.eulerAngles;
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyUp("escape")) {
			Screen.fullScreen = false;
			//shrink resolution?
		}
		if(Input.GetKeyUp(KeyCode.F)) {
			Screen.fullScreen = true;
			//other resolution settings?
		}

		if(Input.GetKeyUp("space") && startScreen == true && avail == false) {
			transition = true;
			Camera.main.GetComponent<DepthOfField>().focalTransform = sword.transform;
		}

		if(transition == true && avail == false) {
			StartCoroutine(moveObject(gameObject.transform, startPos, endPos, 2.2f));
		}
	}

	IEnumerator moveObject(Transform thisTransform, Vector3 startPos, Vector3 endPos, float time){
		avail = true;
		float i = 0.0f;
		var rate = 1.5f/time;
		while (i < 1.5f) {
			i += Time.deltaTime * rate;
			thisTransform.position = Vector3.Lerp(startPos, endPos, i);

			Vector3 rotLerp = Vector3.Lerp(startRot, endRot, i);
			Quaternion rotTemp = Quaternion.Euler(rotLerp.x,rotLerp.y,rotLerp.z);

			thisTransform.rotation = rotTemp;

			yield return null;
		}
		if(i >= 1.5f) {
			yield return new WaitForSeconds(0.3f);
			sword.GetComponent<SwordGen>().startRespawn();
			sword.GetComponent<SwordGen>().inputEnabled = true;
			yield return null;
		}
	}

}









