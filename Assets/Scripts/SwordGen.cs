﻿using UnityEngine;
using System.Collections;
using System.IO;

public class SwordGen : MonoBehaviour {
	
	public enum WeaponSize {Small = 0, Medium = 1, Large = 2};
	public ParticleSystem particleBurst;
	public AudioSource audioEffect;

	public bool randomized = true;
	public WeaponSize setSize = 0;
	public int setBlade = 0;
	public int setGuard = 0;
	public int setHandle = 0;
	public bool interaction = true;

	public bool inputEnabled = false;

	WeaponSize weaponSize;
	int bladeNum, guardNum, handleNum;
	int bladeIndex, guardIndex, handleIndex;
	string bladePath, guardPath, handlePath;
	Vector3 startLoc;
	float startTime = 0;
	bool genEnabled = true;

	public GameObject[] smallBlades;
	public GameObject[] smallGuards;
	public GameObject[] smallHandles;

	public GameObject[] medBlades;
	public GameObject[] medGuards;
	public GameObject[] medHandles;

	public GameObject[] largeBlades;
	public GameObject[] largeHandles;


	Color[] bladeColors = {
		new Color(190/255.0f, 190/255.0f, 190/255.0f,1.0f), //0 - steel
		new Color(110/255.0f, 110/255.0f, 110/255.0f,1.0f), //1 - iron
		new Color(15/255.0f,15/255.0f,15/255.0f,1.0f), 		//2 - obsidian
		new Color(52/255.0f,190/255.0f,80/255.0f,1.0f), 	//3 - jade
		new Color(150/255.0f, 210/255.0f, 230/255.0f,1.0f), //4 - mithril
		new Color(76/255.0f,255/255.0f,249/255.0f,1.0f),	//5 - orichalcum
		new Color(255/255.0f,60/255.0f,46/255.0f,1.0f), 	//6 - flamesteel
		new Color(85/255.0f,15/255.0f,15/255.0f,1.0f), 		//7 - bloodsteel
		new Color(255/255.0f,60/255.0f,155/255.0f,1.0f) 	//8 - light pink
	};

	Color[] guardColors = {
		new Color(188/255.0f,133/255.0f,84/255.0f), 		//0 - light wood
		new Color(97/255.0f,65/255.0f,55/255.0f), 			//1 - medium wood
		new Color(67/255.0f,35/255.0f,25/255.0f), 			//2 - dark wood
		new Color(15/255.0f,15/255.0f,15/255.0f,1.0f), 		//3 - obsidian
		new Color(52/255.0f,190/255.0f,80/255.0f,1.0f),		//4 - jade
		new Color(255/255.0f,169/255.0f,39/255.0f,1.0f),	//5 - gold
		new Color(167/255.0f,128/255.0f, 26/255.0f,1.0f),	//6 - bronze
		new Color(175/255.0f,155/255.0f, 145/255.0f,1.0f),	//7 - silver
		new Color(255/255.0f,5/255.0f,95/255.0f,1.0f) 		//8 - hot pink
	};

	Color[] handleColors = {
		new Color(188/255.0f,133/255.0f,84/255.0f), 		//0 - light wood
		new Color(97/255.0f,65/255.0f,55/255.0f),   		//1 - medium wood
		new Color(67/255.0f,35/255.0f,25/255.0f), 			//2 - dark wood
		new Color(15/255.0f,15/255.0f,15/255.0f,1.0f),		//3 - obsidian
		new Color(52/255.0f,190/255.0f,80/255.0f,1.0f),		//4 - jade
		new Color(255/255.0f,169/255.0f,39/255.0f,1.0f),	//5 - gold
		new Color(167/255.0f,128/255.0f, 26/255.0f,1.0f),	//6 - bronze
		new Color(175/255.0f,155/255.0f, 145/255.0f,1.0f),	//7 - silver
		new Color(255/255.0f,5/255.0f,95/255.0f,1.0f) 		//8 - hot pink
	};
	
	void Start () {
		startLoc = gameObject.transform.position;
		if(randomized) {
			//generateSword();
		} else {
			generateSword(setSize,setBlade,setGuard,setHandle);
		}
	}

	void Update() {
		if(Input.GetKeyUp("space") && interaction == true && inputEnabled) {
			if(genEnabled == true) {
				startRespawn();
			}
		}
		if(interaction == true && inputEnabled) {
			startTime += Time.deltaTime;
			gameObject.transform.position += new Vector3(0.0f, .002f * Mathf.Cos(startTime * .95f), 0.0f);
		}
	}

	public void startRespawn() {
		particleBurst.Play();
		audioEffect.Play();
		respawnSword();
	}
	
	void generateSword() {

		GameObject blade, guard, handle;
		blade = new GameObject();
		guard = new GameObject();
		handle = new GameObject();
		
		//weaponSize = (WeaponSize)Random.Range(0, 3);
		int sizeTemp = Random.Range (0, 10);
		if (sizeTemp < 4) {
			weaponSize = WeaponSize.Small;
		} else if (sizeTemp >= 4 && sizeTemp < 7) {
			weaponSize = WeaponSize.Medium;
		} else if (sizeTemp >= 7) {
			weaponSize = WeaponSize.Large;
		}

		if (weaponSize == WeaponSize.Medium) {
			gameObject.transform.position += new Vector3 (.18f, 0.0f, 0.0f);
		} else if (weaponSize == WeaponSize.Large) {
			gameObject.transform.position += new Vector3 (.28f, 0.0f, 0.0f);
		}

		/*
		bladePath = "Prefabs/SwordComponents/" + weaponSize.ToString();
		guardPath = "Prefabs/SwordComponents/" + weaponSize.ToString();
		handlePath = "Prefabs/SwordComponents/" + weaponSize.ToString();
		
		DirectoryInfo directory = new DirectoryInfo("Assets/Resources/" + bladePath + "/");
		bladeNum = directory.GetFiles(weaponSize.ToString() + "Blade*.prefab").Length; 
		directory = new DirectoryInfo("Assets/Resources/" + guardPath + "/");
		guardNum = directory.GetFiles(weaponSize.ToString() + "Guard*.prefab").Length;
		directory = new DirectoryInfo("Assets/Resources/" + handlePath + "/");
		handleNum = directory.GetFiles(weaponSize.ToString() + "Handle*.prefab").Length;
		
		//Debug.Log("Assets/Resources/" + handlePath + "/" + weaponSize.ToString() + "Handle*.prefab");
		
		if(bladeNum == 0 || guardNum == 0 || handleNum == 0){ Debug.Log("Missing component in folder"); return; }
		
		bladeIndex = Random.Range(0, bladeNum);
		guardIndex = Random.Range (0, guardNum);
		handleIndex = Random.Range(0, handleNum);
		Debug.Log ("Generated Sword Parts: " + bladeIndex.ToString() + "-" + guardIndex.ToString() + "-" + handleIndex.ToString());
		
		blade = Instantiate(Resources.Load (bladePath + "/" + weaponSize.ToString() + "Blade" + (bladeIndex).ToString()) as GameObject);
		guard = Instantiate(Resources.Load (guardPath + "/" + weaponSize.ToString() + "Guard" + (guardIndex).ToString() ) as GameObject);
		handle = Instantiate(Resources.Load (handlePath + "/" + weaponSize.ToString() + "Handle" + (handleIndex).ToString()) as GameObject);
		*/

		//For web player
		if (weaponSize == WeaponSize.Small) {
			Destroy(blade); Destroy(guard); Destroy(handle);
			blade = Instantiate (smallBlades[Random.Range(0, smallBlades.Length)]);
			guard = Instantiate (smallGuards [Random.Range (0, smallGuards.Length)]);
			handle = Instantiate (smallHandles [Random.Range (0, smallHandles.Length)]);
		} else if (weaponSize == WeaponSize.Medium) {
			Destroy(blade); Destroy(guard); Destroy(handle);
			blade = Instantiate (medBlades [Random.Range(0, medBlades.Length)]);
			guard = Instantiate (medGuards [Random.Range (0, medGuards.Length)]);
			handle = Instantiate (medHandles [Random.Range (0, medHandles.Length)]);
		} else if (weaponSize == WeaponSize.Large) {
			Destroy(blade); Destroy(guard); Destroy(handle);
			blade = Instantiate (largeBlades[Random.Range(0, largeBlades.Length)]);
			guard = Instantiate (medGuards[Random.Range (0, medGuards.Length)]);
			handle = Instantiate (largeHandles[Random.Range (0, largeHandles.Length)]);
		}

		//End web player

		MeshRenderer bRend = blade.GetComponent<MeshRenderer>();
		MeshRenderer gRend = guard.GetComponent<MeshRenderer>();
		MeshRenderer hRend = handle.GetComponent<MeshRenderer>();
		
		//COMBO PICKER - SETS RATIOS FOR RARE BLADE PARTS
		//50 matched wood
		//10 rock
		//30 metal
		//8  mixed wood
		//1  mixed rock
		//1  pink
		int bColor = 0, gColor = 0, hColor = 0;
		int hiltMatch = Random.Range(0, 100);
		if(hiltMatch <  50) {
			gColor = Random.Range(0,3); //Matched Wood 0,1,2
			hColor = gColor;
		} else if(hiltMatch >= 50 && hiltMatch < 60) {
			gColor = Random.Range(3,5); //Matched Rock 3,4
			hColor = gColor;
		} else if(hiltMatch >= 60 && hiltMatch < 90) {
			gColor = Random.Range(5,8); //Matched Metal 5,6,7
			hColor = gColor;
		} else if(hiltMatch >= 90 && hiltMatch < 91) {
			gColor = 3;					//Mixed Obsidian and Jade 3,4
			hColor = 4;
		} else if(hiltMatch >= 91 && hiltMatch < 99) {
			gColor = Random.Range(5,7); //Mixed Wood 5,6,7
			hColor = (gColor + 1);
		} else if(hiltMatch == 99) {
			gColor = 8;					//Matched Pink 8
			hColor = 8;
		}
		
		int bladeMatch = Random.Range(0,100);
		if(bladeMatch <  50) {
			bColor = Random.Range(0, 2);
		} else if(bladeMatch >= 50 && bladeMatch < 60) {
			bColor = Random.Range(2, 4);
		} else if(bladeMatch >= 60 && bladeMatch < 99) {
			bColor = Random.Range(4, 8);
		} else if(bladeMatch == 99) {
			bColor = 8;
		}
		
		bRend.material.color = bladeColors[bColor];
		if(bRend.materials.Length > 1) {
			for(int i = 0; i < bRend.materials.Length; i++) {
				bRend.materials[i].color = bladeColors[bColor];
			}
		}
		gRend.material.color = guardColors[gColor];
		hRend.material.color = handleColors[hColor];
		
		//gameObject.transform.position = new Vector3(-5.5f,0.0f,0.807f);
		blade.transform.parent = gameObject.transform;
		blade.transform.localPosition = new Vector3(0,0,0);
		guard.transform.parent = gameObject.transform;
		guard.transform.localPosition = new Vector3(0,0,0);
		handle.transform.parent = gameObject.transform;
		handle.transform.localPosition = new Vector3(0,0,0);

		gameObject.transform.Rotate(new Vector3(-90.0f, 0.0f, 90.0f));
	}

	//For non-randomized weapons
	void generateSword(WeaponSize s, int b, int g, int h) {
		
		GameObject blade, guard, handle;
		blade = new GameObject();
		guard = new GameObject();
		handle = new GameObject();
		
		weaponSize = s;
		
		if (weaponSize == WeaponSize.Medium) {
			gameObject.transform.position += new Vector3 (.18f, 0.0f, 0.0f);
		} else if (weaponSize == WeaponSize.Large) {
			gameObject.transform.position += new Vector3 (.28f, 0.0f, 0.0f);
		}
		
		//For web player
		if (weaponSize == WeaponSize.Small) {
			Destroy(blade); Destroy(guard); Destroy(handle);
			blade = Instantiate (smallBlades[b]);
			guard = Instantiate (smallGuards [g]);
			handle = Instantiate (smallHandles [h]);
		} else if (weaponSize == WeaponSize.Medium) {
			Destroy(blade); Destroy(guard); Destroy(handle);
			blade = Instantiate (medBlades [b]);
			guard = Instantiate (medGuards [g]);
			handle = Instantiate (medHandles [h]);
		} else if (weaponSize == WeaponSize.Large) {
			Destroy(blade); Destroy(guard); Destroy(handle);
			blade = Instantiate (largeBlades[b]);
			guard = Instantiate (medGuards[g]);
			handle = Instantiate (largeHandles[h]);
		}
		//End web player
		
		MeshRenderer bRend = blade.GetComponent<MeshRenderer>();
		MeshRenderer gRend = guard.GetComponent<MeshRenderer>();
		MeshRenderer hRend = handle.GetComponent<MeshRenderer>();
		
		//COMBO PICKER - SETS RATIOS FOR RARE BLADE PARTS
		//50 matched wood
		//10 rock
		//30 metal
		//8  mixed wood
		//1  mixed rock
		//1  pink
		int bColor = 0, gColor = 0, hColor = 0;
		int hiltMatch = Random.Range(0, 100);
		if(hiltMatch <  50) {
			gColor = Random.Range(0,3); //Matched Wood 0,1,2
			hColor = gColor;
		} else if(hiltMatch >= 50 && hiltMatch < 60) {
			gColor = Random.Range(3,5); //Matched Rock 3,4
			hColor = gColor;
		} else if(hiltMatch >= 60 && hiltMatch < 90) {
			gColor = Random.Range(5,8); //Matched Metal 5,6,7
			hColor = gColor;
		} else if(hiltMatch >= 90 && hiltMatch < 91) {
			gColor = 3;					//Mixed Obsidian and Jade 3,4
			hColor = 4;
		} else if(hiltMatch >= 91 && hiltMatch < 99) {
			gColor = Random.Range(5,7); //Mixed Wood 5,6,7
			hColor = (gColor + 1);
		} else if(hiltMatch == 99) {
			gColor = 8;					//Matched Pink 8
			hColor = 8;
		}
		
		int bladeMatch = Random.Range(0,100);
		if(bladeMatch <  50) {
			bColor = Random.Range(0, 2);
		} else if(bladeMatch >= 50 && bladeMatch < 60) {
			bColor = Random.Range(2, 4);
		} else if(bladeMatch >= 60 && bladeMatch < 99) {
			bColor = Random.Range(4, 8);
		} else if(bladeMatch == 99) {
			bColor = 8;
		}
		
		bRend.material.color = bladeColors[bColor];
		if(bRend.materials.Length > 1) {
			for(int i = 0; i < bRend.materials.Length; i++) {
				bRend.materials[i].color = bladeColors[bColor];
			}
		}
		gRend.material.color = guardColors[gColor];
		hRend.material.color = handleColors[hColor];
		
		//gameObject.transform.position = new Vector3(-5.5f,0.0f,0.807f);
		blade.transform.parent = gameObject.transform;
		blade.transform.localPosition = new Vector3(0,0,0);
		blade.transform.rotation = gameObject.transform.rotation;
		blade.transform.localScale = new Vector3(1, 1, 1);//blade.transform.parent.localScale;
		guard.transform.parent = gameObject.transform;
		guard.transform.localPosition = new Vector3(0,0,0);
		guard.transform.rotation = gameObject.transform.rotation;
		guard.transform.localScale = new Vector3(1, 1, 1);//blade.transform.parent.localScale;
		handle.transform.parent = gameObject.transform;
		handle.transform.localPosition = new Vector3(0,0,0);
		handle.transform.rotation = gameObject.transform.rotation;
		handle.transform.localScale = new Vector3(1, 1, 1);//blade.transform.parent.localScale;
		
		//gameObject.transform.Rotate(new Vector3(-90.0f, 0.0f, 90.0f));
	}

	public void respawnSword() {
		genEnabled = false;
		foreach (Transform child in gameObject.transform) {
			GameObject.Destroy(child.gameObject);
		}

		gameObject.transform.position = startLoc;
		startTime = 0;
		StartCoroutine(delayedGenerate());
	}

	IEnumerator  delayedGenerate() {
		yield return new WaitForSeconds(0.06f);
		generateSword();
		genEnabled = true;
	}
}





