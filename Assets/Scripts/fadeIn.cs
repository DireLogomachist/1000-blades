﻿using UnityEngine;
using System.Collections;

public class fadeIn : MonoBehaviour {

	public float waitTime = 1.0f;
	public float fadeTime = 1.0f;

	// Use this for initialization
	void Start () {
		StartCoroutine(fade());
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	IEnumerator fade() {
		float i = 0;
		MeshRenderer rend = gameObject.GetComponent<MeshRenderer>();

		yield return new WaitForSeconds(waitTime);
		while(i < fadeTime) {
			i += Time.deltaTime;

			rend.material.color = new Color(rend.material.color.r,rend.material.color.g,rend.material.color.b,i/fadeTime);

			yield return null;
		}

	}

}
